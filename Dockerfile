FROM java:8
ADD target/nbos.jar nbos.jar
ENTRYPOINT ["java","-jar","nbos.jar"]