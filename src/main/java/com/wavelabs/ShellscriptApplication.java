package com.wavelabs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.wavelabs")
public class ShellscriptApplication {

	public static void main(String[] args) {
		SpringApplication.run(ShellscriptApplication.class, args);

	}
}
