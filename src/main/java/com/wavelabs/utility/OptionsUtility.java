package com.wavelabs.utility;

import org.apache.commons.cli.Options;

/**
 * This class Command arguments options for different commands
 * 
 * @author gopikrishnag
 *
 */
public class OptionsUtility {

	public static Options getOptionsForSignUp() {
		Options options = new Options();
		options.addOption("fn", true, "Give correct first name");
		options.addOption("ln", true, "Enter correct last name");
		options.addOption("email", true, "Enter correct Email");
		options.addOption("password", true, "Enter correct password");
		options.addOption("fn", true, "list the all commands");
		return options;
	}

	public static Options getOptionsForLogIn() {
		Options options = new Options();
		options.addOption("u", true, "Username");
		options.addOption("p", true, "password");
		return options;
	}

	public static Options getOptionsForTenantCreation() {

		Options options = new Options();
		options.addOption("t", true, "Tenant Name");
		return options;

	}

	public static Options getOptionsForCreateClient() {

		Options options = new Options();
		options.addOption("cn",true, "Client Name");
		options.addOption("cs", true,"Client Secret");
		options.addOption("t", true,"Tenant id");
		return options;
	}
}
