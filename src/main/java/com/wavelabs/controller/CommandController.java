package com.wavelabs.controller;

import java.io.IOException;

import org.apache.commons.cli.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wavelabs.service.CommandService;

/**
 * 
 * @author gopikrishnag
 *
 */
@RestController
public class CommandController {

	@Autowired
	private CommandService service;

	@RequestMapping("/{command}")
	public String parseCommand(@RequestParam("args") String[] args, @PathVariable("command") String command)
			throws ParseException, IOException {
		if ("signup".equalsIgnoreCase(command)) {
			return "\n" + service.signUpUser(args);
		} else if ("login".equals(command)) {
			return "\n" + service.loginUser(args);
		} else if ("create-tenant".equals(command)) {
			return "\n" + service.createTenant(args);
		} else if ("create-client".equals(command)) {
			return service.createClient(args);
		} else if ("show-clients".equals(command)) {
			
		} else if ("logout".equals(command)) {
			return service.logout();
		}
		else if("show-tenants".equals(command)) {
			
		}
		return null;
	}
}
